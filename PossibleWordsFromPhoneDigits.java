import java.util.ArrayList;
import java.util.List;

class Solution {
    private static final String[] KEYS = {
        "",     // 0
        "",     // 1
        "abc",  // 2
        "def",  // 3
        "ghi",  // 4
        "jkl",  // 5
        "mno",  // 6
        "pqrs", // 7
        "tuv",  // 8
        "wxyz"  // 9
    };

    public List<String> letterCombinations(String digits) {
        List<String> result = new ArrayList<>();
        if (digits == null || digits.length() == 0) {
            return result;
        }
        int[] nums = new int[digits.length()];
        for (int i = 0; i < digits.length(); i++) {
            nums[i] = Character.getNumericValue(digits.charAt(i));
        }
        backtrack(nums, 0, new StringBuilder(), result);
        return result;
    }

    private void backtrack(int[] digits, int index, StringBuilder current, List<String> result) {
        if (index == digits.length) {
            result.add(current.toString());
            return;
        }
        String letters = KEYS[digits[index]];
        for (int i = 0; i < letters.length(); i++) {
            current.append(letters.charAt(i));
            backtrack(digits, index + 1, current, result);
            current.deleteCharAt(current.length() - 1);
        }
    }
}

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        String digits = "234"; // Example input
        List<String> combinations = solution.letterCombinations(digits);
        for (String combination : combinations) {
            System.out.print(combination + " ");
        }
    }
}
